
public class MarksArray {
	public static void main(String[] args) {

		int[] AnneMarks = { 75, 90, 100, 78, 100 };
		int[] BredMarks = { 100, 95, 90, 80, 95 };
		int[] LeeMarks = { 95, 100, 90, 80, 70 };
		int[] KumarMarks = { 90, 90, 75, 80, 70 };
		int[] RaviMarks = { 80, 85, 80, 75, 70 };

		System.out.println("total of anne marks is : " + MarksArray2.findTotal(AnneMarks));
		System.out.println("total of bred marks is : " + MarksArray2.findTotal(BredMarks));
		System.out.println("total of lee marks is : " + MarksArray2.findTotal(LeeMarks));
		System.out.println("total of kumar marks is : " + MarksArray2.findTotal(KumarMarks));
		System.out.println("total of ravi marks is : " + MarksArray2.findTotal(RaviMarks));

		System.out.println("maximum of anne marks  : " + MarksArray2.findMax(AnneMarks));
		System.out.println("maximum of bred marks  : " + MarksArray2.findMax(BredMarks));
		System.out.println("maximum of lee marks  : " + MarksArray2.findMax(LeeMarks));
		System.out.println("maximum of kumar marks  : " + MarksArray2.findMax(KumarMarks));
		System.out.println("maximum of ravi marks  : " + MarksArray2.findMax(RaviMarks));

		System.out.println("minimum of anne marks is : " + MarksArray2.findMin(AnneMarks));
		System.out.println("minimum of bred marks is : " + MarksArray2.findMin(BredMarks));
		System.out.println("minimum of lee marks is : " + MarksArray2.findMin(LeeMarks));
		System.out.println("minimum of kumar marks is : " + MarksArray2.findMin(KumarMarks));
		System.out.println("minimum of ravi marks is : " + MarksArray2.findMin(RaviMarks));

		System.out.println("average of anne marks is : " + MarksArray2.findavg(AnneMarks));
		System.out.println("average  of bred marks is : " + MarksArray2.findavg(BredMarks));
		System.out.println("average  of lee marks is : " + MarksArray2.findavg(LeeMarks));
		System.out.println("average  of kumar marks is : " + MarksArray2.findavg(KumarMarks));
		System.out.println("average  of ravi marks is : " + MarksArray2.findavg(RaviMarks));

	}

}
