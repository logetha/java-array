
public class MarksArray2 {
	static int total = 0;

	public static int findTotal(int[] marks) {
		int total = 0;
		for (int mark : marks) {
			total += mark;
		}
		return total;
	}

	public static int findMax(int[] marks) {
		int max = marks[0];
		for (int mark : marks) {
			if (max > mark) {
				max = mark;
			}
		}
		return max;
	}

	public static int findMin(int[] marks) {
		int min = marks[0];
		for (int mark : marks) {
			if (min > mark) {
				min = mark;
			}
		}
		return min;
	}

	public static double findavg(int[] marks) {
		return findTotal(marks) / Double.parseDouble(marks.length + "");
	}
}