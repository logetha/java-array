import java.util.Random;

public class MarksRandom2 {
	public static void createRandomArray(int size, int range) {
		Random ran = new Random();
		int[] ranArray = new int[size];
		for (int i = 0; i < size; i++) {
			ranArray[i] = ran.nextInt(range);

		}
		printRandomArray(ranArray);
	}

	private static void printRandomArray(int[] array) {
		for (int element : array) {
			System.out.print(element + ",");
		}
	}

	public static void createRandomMarks(int size, int range) {
		Random ran = new Random();
		int[] ranArray = new int[size];
		for (int i = 0; i < size; i++) {
			ranArray[i] = ran.nextInt(range);

		}
		return;
	}
}
